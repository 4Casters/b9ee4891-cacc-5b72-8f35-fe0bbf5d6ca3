import logging
import time
import http.client
import json
import importlib as implib

from src.tradeapi import TradeApi
import sys
from pandas.io.json import json_normalize
import pandas as pd
import numpy as np
from pyfolio import tears
import pyfolio.timeseries as ts
import empyrical as ep
import scipy.stats as stats
import os


class Context(object):
    pass


CI = os.environ.get("CI")
host = "localhost"
if CI is not None and CI:
    host = "backtest"

logging.basicConfig(level=logging.INFO)
algo = implib.import_module(sys.argv[1])

context = Context()
algo.init(context)
ccy_pair = context.ccy_pair
freq = context.freq
start_date = context.start_date
end_date = context.end_date
size = context.size
session = http.client.HTTPConnection(host + ':5655', timeout=300)
my_url = "/actions/new-backtest/dates/{:s}_{:s}/identifiers/{:s}-{:s}".format(start_date, end_date, ccy_pair, freq).strip('\n')
logging.info(my_url)
session.request('GET', my_url)
ret = json.loads(session.getresponse().read().decode())
session.close()

backtestId = ret['id']   #backtestConfigInfo.id
backtestPort = ret['port']   #backtestConfigInfo.port
origin = "/backtests/{:s}".format(backtestId)
logging.debug(origin)
url_next = origin + '/actions/next'
session = http.client.HTTPConnection('localhost', port=backtestPort, timeout=300)
trade_api = TradeApi(session, origin)
context.tradeapi = trade_api
completed = False
start = time.time()
context.index = -1
context.hasPosition = False
context.data = np.zeros(size, dtype=[('open', 'float'), ('low', 'float'), ('high', 'float'), ('close', 'float')])
context.timestamps = np.zeros(size)
context.last_ccy_pair = ''

while not completed:
    url = url_next
    session.request('GET', url)
    resp = session.getresponse().read().decode()
    if resp != "":
        context.index = context.index + 1
        index = context.index
        ret = json.loads(resp)
        t = ret['type']
        if t == "QUOTE":
            #logging.debug("quote")
            local_df = ret['value']
            if context.index >= size:
                context.data = np.roll(context.data, -1, axis=0)
                context.data['open'][size-1] = local_df['open']
                context.data['low'][size-1] = local_df['low']
                context.data['high'][size-1] = local_df['high']
                context.data['close'][size-1] = local_df['close']
                context.last_ccy_pair = local_df['identifier']
                context.timestamps = np.roll(context.timestamps, -1)
                context.timestamps[size-1] = local_df['timestamp']
                algo.on_quote(context)
            else:
                context.data['open'][index] = local_df['open']
                context.data['low'][index] = local_df['low']
                context.data['high'][index] = local_df['high']
                context.data['close'][index] = local_df['close']
                context.last_ccy_pair = local_df['identifier']
                context.timestamps[index] = local_df['timestamp']

        elif t == "RESULTS":
            context.results = ret['results']
            algo.on_results(context, ret)
            completed = True
            with open('ret.json', 'w') as outfile:
                json.dump(ret, outfile, indent=4)

        if context.index % 1440 == 0:
            logging.info('index: ' + str(context.index))

hrend = time.time() - start
session.close()
logging.info('Execution time (hr): {}'.format(hrend))
logging.info(context.results)
feed = context.results['feed_EURUSD-1M']
df = json_normalize(feed['orders'])
#logging.info(df)
df.timestamp = pd.to_datetime(df.timestamp, unit='ms')
df.price = pd.to_numeric(df.price)
df.quantity = pd.to_numeric(df.quantity)
trades = df.rename(columns={'timestamp': 'date_time'}, inplace=False)
logging.info(trades)
initial_amount = 0
for ccy_pair in trades['identifier'].unique():
    trades_by_pair = trades.loc[trades['identifier'] == ccy_pair]
    positions_by_pair = trades_by_pair[['date_time', 'price', 'quantity']]
    positions_by_pair['positions'] = -1 * positions_by_pair['price'] * positions_by_pair['quantity']
    positions_by_pair['closed'] = positions_by_pair['positions']\
        .rolling(min_periods=1, window=2)\
        .sum()
    positions_by_pair['date'] = positions_by_pair['date_time'].dt.date
    closed_positions_by_pair_and_date = positions_by_pair.iloc[1::2, [4, 5]]
    daily_closed_positions_by_pair_and_date = closed_positions_by_pair_and_date.groupby(['date']).sum()

    logging.info(daily_closed_positions_by_pair_and_date)
    daily_closed_positions_by_pair_and_date.to_csv(sys.argv[1] + '_df.csv')
    bootstrap = ts.perf_stats(daily_closed_positions_by_pair_and_date['closed'])
    print(bootstrap)

